program Test;

uses
  Vcl.Forms,
  View.Main in 'views\View.Main.pas' {MainView},
  View.Progress in 'views\View.Progress.pas' {ProgressView};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainView, MainView);
  Application.Run;
end.
