unit View.Main;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  View.Progress;

type
  TMainView = class(TForm)
    btnException: TButton;
    btnSimple: TButton;
    btnExceptionSilent: TButton;
    procedure btnExceptionClick(Sender: TObject);
    procedure btnSimpleClick(Sender: TObject);
    procedure btnExceptionSilentClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainView: TMainView;

implementation

{$R *.dfm}

procedure TMainView.btnExceptionClick(Sender: TObject);
begin
  TProgressView.Execute(Self,
    procedure ()
    begin
      Sleep(2000);
      raise Exception.Create('Some exception');
    end);
end;

procedure TMainView.btnExceptionSilentClick(Sender: TObject);
begin
  try
  TProgressView.Execute(Self,
    procedure ()
    begin
      Sleep(2000);
      raise Exception.Create('Some exception');
    end);
  except
    on E: Exception do begin
      // Skip all of exceptions.
    end;
  end;
end;

procedure TMainView.btnSimpleClick(Sender: TObject);
begin
  TProgressView.Execute(Self,
    procedure ()
    begin
      Sleep(2000);
    end);
end;

end.
