unit View.Progress;

interface

uses
  System.SysUtils,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.WinXCtrls,
  Vcl.StdCtrls;

type
  TProgressView = class(TForm)
    actWait: TActivityIndicator;
    lblText: TLabel;
  strict private
  type
    {$REGION 'TWorker'}
    TWorker = class(TThread)
    strict private
    var
      FProc       : TProc;
      FException  : TObject;
    protected
      procedure Execute(); override;
    public
      constructor Create(const AProc: TProc);

      property InternalException: TObject read FException;
    end;
    {$ENDREGION 'TWorker'}
  var
    FWorker     : TWorker;
    FException  : TObject;
    procedure CreateWorker(const AProc: TProc);
    procedure WorkerTerminate(Sender: TObject);
  protected
    procedure DoShow(); override;
  public
    constructor Create(AOwner: TComponent); override;

    function CloseQuery(): Boolean; override;

    class procedure Execute(const AOwner: TForm; const AProc: TProc); static;
  end;

implementation

{$R *.dfm}

{ TProgressView.TWorker }

constructor TProgressView.TWorker.Create(const AProc: TProc);
begin
  ASSERT(Assigned(AProc));
  inherited Create(True);
  FProc := AProc;
  FreeOnTerminate := True;
end;

procedure TProgressView.TWorker.Execute();
begin
  try
    FProc();
  except
    FException := AcquireExceptionObject;
  end;
end;

{ TProgressView }

class procedure TProgressView.Execute(const AOwner: TForm; const AProc: TProc);
var
  LView      : TProgressView;
  LException : TObject;
begin
  ASSERT(Assigned(AProc));
  LView := TProgressView.Create(AOwner);
  try
    LView.CreateWorker(AProc);
    LView.ShowModal();
    LException := LView.FException;
  finally
    FreeAndNil(LView);
  end;
  if (LException <> nil) then begin
    raise LException;
  end;
end;

constructor TProgressView.Create(AOwner: TComponent);
begin
  inherited;
  actWait.Align := TAlign.alLeft;
end;

procedure TProgressView.CreateWorker(const AProc: TProc);
begin
  if (FWorker = nil) then begin
    FWorker := TWorker.Create(AProc);
    FWorker.OnTerminate := WorkerTerminate;
  end;
end;

procedure TProgressView.WorkerTerminate(Sender: TObject);
begin
  ModalResult := mrOk;
  if (FWorker <> nil) then begin
    FException := FWorker.InternalException;
    FWorker := nil;
  end;
end;

function TProgressView.CloseQuery(): Boolean;
begin
  Result := (inherited) and (FWorker = nil);
end;

procedure TProgressView.DoShow();
begin
  inherited;
  if (FWorker <> nil) then begin
    FWorker.Start();
  end;
end;

end.
