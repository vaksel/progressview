object MainView: TMainView
  Left = 0
  Top = 0
  Caption = 'MainView'
  ClientHeight = 299
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnException: TButton
    Left = 24
    Top = 55
    Width = 140
    Height = 25
    Caption = 'Run with exception'
    TabOrder = 0
    OnClick = btnExceptionClick
  end
  object btnSimple: TButton
    Left = 24
    Top = 24
    Width = 140
    Height = 25
    Caption = 'Run simple'
    TabOrder = 1
    OnClick = btnSimpleClick
  end
  object btnExceptionSilent: TButton
    Left = 24
    Top = 86
    Width = 140
    Height = 25
    Caption = 'Run with silent exception'
    TabOrder = 2
    OnClick = btnExceptionSilentClick
  end
end
